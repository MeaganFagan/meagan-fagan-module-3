import 'package:flutter/material.dart';
import 'package:flutter_application_1/home_page.dart';
import 'login_page.dart';
import 'user_profile.dart';
import 'home_page.dart';
import 'feature_screenone.dart';
import 'feature_screentwo.dart';

class feature_screenone extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Text('Feature Screen 1'),
      ),
      body: Center(
        child: RaisedButton(
          color: Colors.blue,
          child: Text('Go Back To home page'),
          onPressed: () {
            Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return home_page();
                    },
                  ),
                );
          },
        ),
      ),
    );
  }
}